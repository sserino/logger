FROM node:8.0.0-alpine

RUN mkdir -p /logger
ADD package.json server.js /logger/
WORKDIR /logger
RUN npm install -g -s --no-progress yarn && \
    yarn && \
    yarn cache clean
ENV LOG_DIR_PATH /logger/log
CMD ["yarn", "start"]
EXPOSE 6200
