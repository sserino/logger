var express = require('express');
var bodyParser = require('body-parser');
var winston = require('winston');
var path = require('path');
var fs = require('fs');

var logDirPath = process.env.LOG_DIR_PATH || 'log';
var logDir = path.resolve(__dirname, logDirPath);
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}
var logFile = path.join(logDir, 'app.log');
console.log('Log file: ' + logFile);
const logFormatter = function(options) {
  return 'time="' + options.timestamp() + '" level="' + options.level +
    '" message="' + (options.message ? options.message : '') +
    (options.meta && Object.keys(options.meta).length ? ' -- ' + JSON.stringify(options.meta) : '' ) + '"';
};
const timestamp = function() {
  return (new Date).toISOString();
};
var transport = new (winston.transports.File)({
  name: 'app',
  filename: logFile,
  json: false,
  level: process.env.ENV === 'development' ? 'debug' : 'info',
  timestamp: timestamp,
  formatter: logFormatter
});
var logger = new (winston.Logger)({
  transports: [
    transport
  ],
  exitOnError: false
});

logger.log('info', 'Log Server started');

var app = express();
var jsonParser = bodyParser.json();
app.post('/api/logger', jsonParser, function (req, res) {
  if (!req.body) {
    return res.sendStatus(400)
  }
  var level = req.body.level.toLowerCase() || 'error';
  logger.log(level, req.body.message);
  return res.sendStatus(204);
})

var server = app.listen(6200);
